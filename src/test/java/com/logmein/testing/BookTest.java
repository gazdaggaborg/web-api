package com.logmein.testing;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Predicate;

import org.apache.http.client.fluent.Request;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class BookTest {

	private BookClient bookClient;

	@Before
	public void Before() {
 		Properties p = new Properties();
		InputStream inputStream = ClassLoader.getSystemResourceAsStream("setting.properties");
		try {
			p.load(inputStream);
		} catch (Exception e) {
			Assert.fail();
		}

		String baseUrl = p.getProperty("url");
		bookClient = new BookClient(baseUrl);
	}

	

	
	private void assertSame(Book b1, Book b2) {
		Assert.assertEquals(b1.id, b2.id);
		assertEqual(b1, b2);
	}
	
	private void assertEqual(Book b1, Book b2) {
	//	Assert.assertEquals(b1.title, b2.title);
	//	Assert.assertEquals(b1.author, b2.author);

		Assert.assertEquals(b1.isbn, b2.isbn);

	}
	

	@Test
	public void test1_canPost() throws Exception {
		final Book createdBook = new Book();
		createdBook.title = "Eclipse of the Crescent Moon";
		createdBook.author = "Géza Gárdonyi";
		createdBook.isbn = "978963133931";

		BookResponse<Book[]> allExistingBook= bookClient.getAll();
		Book existingBook = Arrays.stream(allExistingBook.getResult()).filter(new Predicate<Book>() {
			@Override
			public boolean test(Book b) {
				
				return b.isbn.equals(createdBook.isbn);
			}
		}).findFirst().orElse(null);
		if (existingBook != null) {
			bookClient.delete(existingBook.isbn);
		}

		BookResponse<?> post = bookClient.post(createdBook);

		Assert.assertEquals(200, post.getHttpCode());
	
	
	}

	@Test
	public void test2_canGet() throws Exception {

		BookResponse<Book[]> allBook = bookClient.getAll();

		Assert.assertEquals(200, allBook.getHttpCode());

		int id = allBook.getResult()[0].id;
		
		BookResponse<Book> b = bookClient.get(id);
		Assert.assertEquals(200, b.getHttpCode());

		assertSame(allBook.getResult()[0], b.getResult());
	}
	
	
	
	
	@Test
	public void test3_canPut() throws Exception {

			final Book createdBook = new Book();
			createdBook.title = "Eclipse of the Crescent Moon";
			createdBook.author = "Gézacska Gárdonyi";
			createdBook.isbn = "978963133931";

			
			BookResponse<Book[]> allExistingBook= bookClient.getAll();
			Book existingBook = Arrays.stream(allExistingBook.getResult()).filter(new Predicate<Book>() {
				@Override
				public boolean test(Book b) {
					
					return b.isbn.equals(createdBook.isbn);
				}
			}).findFirst().orElse(null);


			
			BookResponse<?> put = bookClient.put(existingBook.id,createdBook);
			Assert.assertEquals(200, put.getHttpCode());
			

	
	}
	
	@Test
	public void test4_canDelete() throws Exception {
		final Book createdBook = new Book();
		createdBook.title = "Eclipse of the Crescent Moon";
		createdBook.author = "Géza Gárdonyi";
		createdBook.isbn = "978963133931";



		BookResponse<Book[]> allBook = bookClient.getAll();
		final Optional<Book> optionalBook = Arrays.stream(allBook.getResult()).filter(new Predicate<Book>() {
			@Override
			public boolean test(Book b) {
				return b.isbn != null && b.isbn.equals(createdBook.isbn);
			}
		}).findFirst();
		
		
		Assert.assertTrue(optionalBook.isPresent());
		assertEqual(createdBook, optionalBook.get());
		
		
		BookResponse<?> deleteResponse = bookClient.delete(optionalBook.get().id);
		Assert.assertEquals(200, deleteResponse.getHttpCode());

		BookResponse<Book[]> remainingBook = bookClient.getAll();
		boolean deleted = Arrays.stream(remainingBook.getResult()).allMatch(new Predicate<Book>() {

			@Override
			public boolean test(Book b) {
				return b.isbn != optionalBook.get().isbn;
			}

		});

		Assert.assertTrue(deleted); 
	}
//Post testing missing 
	@Test
	public void TestMissingTitle() throws Exception {
		
		Book b = new Book();
		b.title = "";
		b.author = "Géza Gárdonyi";
		b.isbn = "978963133931";
		
		
		BookResponse<Object> response = bookClient.post(b);
		
		Assert.assertEquals(400, response.getHttpCode());
		
	}
	
	@Test
	public void TestMissingISBN() throws Exception {
		
		
		
		Book b = new Book();
		b.title = "Eclipse of the Crescent Moon";
		b.author = "Géza Gárdonyi";
		b.isbn = "";
		
		
		BookResponse<Object> response = bookClient.post(b);
		
		Assert.assertEquals(400, response.getHttpCode());
		
	}
	
	@Test
	public void TestMissingAuthor() throws Exception {

		Book b = new Book();
		b.title = "Eclipse of the Crescent Moon";
		b.author = "";
		b.isbn = "978963133931";
		
		
		BookResponse<Object> response = bookClient.post(b);
		
		Assert.assertEquals(400, response.getHttpCode());
		
	}

	@Test
	public void testMissingTitle() throws Exception{
		String name = null;
		
		titleValidation(name);
	}
	
	@Test
	public void testMissingAuthor() throws Exception{
		String name = null;
		
		authorValidation(name);
	}
	
	@Test
	public void testMissingIsbn() throws Exception{
		String name = null;
		
		isbnValidation(name);
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private void titleValidation(String name) throws Exception {
		final Book createdBook = new Book();
		createdBook.title = name;
		createdBook.author = "Géza Gárdonyi";
		createdBook.isbn = "9789631339314";
		
		Book existingBook = new Book();
		existingBook.id = 0;
		
		BookResponse<?> postResponse = bookClient.post(createdBook);
		
		Assert.assertEquals(400, postResponse.getHttpCode());
		
		BookResponse<?> putResponse = bookClient.put(existingBook.id,createdBook);
		Assert.assertEquals(400, putResponse.getHttpCode());
	}
	
	
	private void authorValidation(String name) throws Exception {
		final Book createdBook = new Book();
		createdBook.title = "Eclipse of the Crescent Moon";
		createdBook.author = name;
		createdBook.isbn = "9789631339314";
		
		Book existingBook = new Book();
		existingBook.id = 0;
		
		BookResponse<?> postResponse = bookClient.post(createdBook);
		
		Assert.assertEquals(400, postResponse.getHttpCode());
		
		BookResponse<?> putResponse = bookClient.put(existingBook.id,createdBook);
		Assert.assertEquals(400, putResponse.getHttpCode());
	}
	
	private void isbnValidation(String name) throws Exception {
		final Book createdBook = new Book();
		createdBook.title = "Eclipse of the Crescent Moon";
		createdBook.author = "Géza Gárdonyi";
		createdBook.isbn = name;
		
		Book existingBook = new Book();
		existingBook.id = 0;
		
		BookResponse<?> postResponse = bookClient.post(createdBook);
		
		Assert.assertEquals(400, postResponse.getHttpCode());
		
		BookResponse<?> putResponse = bookClient.put(existingBook.id,createdBook);
		Assert.assertEquals(400, putResponse.getHttpCode());
		
		
	}
	
	@Test
	public void testLongTitle () throws Exception {
		StringBuilder stringBuilder = new StringBuilder();
		for(int i  = 0; i< 256;i++)
			stringBuilder.append('a');
	
		titleValidation(stringBuilder.toString());		
	}
	
	@Test
	public void testLongAuthor () throws Exception {
		StringBuilder stringBuilder = new StringBuilder();
		for(int i  = 0; i< 256;i++)
			stringBuilder.append('a');
	
		authorValidation(stringBuilder.toString());		
	}
	
	@Test
	public void testLongisbn () throws Exception {
		StringBuilder stringBuilder = new StringBuilder();
		for(int i  = 0; i< 15;i++)
			stringBuilder.append('a');
	
		isbnValidation(stringBuilder.toString());		
	}
	
	
}
