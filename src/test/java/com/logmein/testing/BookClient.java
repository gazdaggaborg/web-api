package com.logmein.testing;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

public class BookClient {

	private final String baseUrl;
	private final Gson g;
	
	public BookClient(String baseUrl) {
		this.baseUrl = baseUrl;
		g = new Gson();		
	}
	
	
	public com.logmein.testing.BookResponse<Book[]> getAll() throws Exception {
		
		Request request = Request.Get(baseUrl + "/api/4048FA07-2BB5-497E-8CA8-F8D5B3B705EB/book");
	
		return doRequest(request, Book[].class); 
	}
	
	public BookResponse<Book> get(int id) throws Exception {
		Request request = Request.Get(baseUrl + "/api/4048FA07-2BB5-497E-8CA8-F8D5B3B705EB/book/"+ id);
		
		return doRequest(request, Book.class);
	}
	
	public BookResponse<Object> post(Book createdBook) throws Exception{
		Request request = Request.Post(baseUrl + "/api/4048FA07-2BB5-497E-8CA8-F8D5B3B705EB/book/").bodyString(g.toJson(createdBook), ContentType.APPLICATION_JSON);
		
		return doRequest(request, Object.class);
	}
	
	public BookResponse<Object> put(int id,Book createdBook) throws Exception{
		Request request = Request.Put(baseUrl + "/api/4048FA07-2BB5-497E-8CA8-F8D5B3B705EB/book/" + id).bodyString(g.toJson(createdBook), ContentType.APPLICATION_JSON);;
		
		return doRequest(request, Object.class);
	}
	
	public BookResponse<Object> put(Book createdBook) throws Exception{
		Request request = Request.Put(baseUrl + "/api/4048FA07-2BB5-497E-8CA8-F8D5B3B705EB/book/").bodyString(g.toJson(createdBook), ContentType.APPLICATION_JSON);;
		
		return doRequest(request, Object.class);
	}
	
	
	public BookResponse<Object> delete(String isbn) throws Exception{
		Request request = Request.Delete(baseUrl + "/api/4048FA07-2BB5-497E-8CA8-F8D5B3B705EB/book/" + isbn);
		
		return doRequest(request, Object.class);
	}
	
	public BookResponse<Object> delete(int id) throws Exception{
		Request request = Request.Delete(baseUrl + "/api/4048FA07-2BB5-497E-8CA8-F8D5B3B705EB/book/" + id);
		
		return doRequest(request, Object.class);
	}
	
	private <T> BookResponse<T> doRequest(Request request, Class<T> classOfT) throws IOException, ClientProtocolException {
		
		HttpResponse httpResponse = request.execute().returnResponse();
		String content = null;
		HttpEntity e = httpResponse.getEntity();
		if (e != null)
				content = EntityUtils.toString(e);
		
		int httpCode = httpResponse.getStatusLine().getStatusCode();
		
		BookResponse<T> bookResponse = new BookResponse<T>();
		bookResponse.setContent(content);
		bookResponse.setHttpCode(httpCode);
		if (httpCode / 100 == 2 && content != null)
			bookResponse.setResult(g.fromJson(content, classOfT));
		
		return bookResponse;
	}



	
}
